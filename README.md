# singularity-jupyter

Jupyterlab notebook servier in Singularity with Holoviews, Altair, PyViz, 
Matplotlib, PyLantern, etc.

For other packages included in the jupyterlab see the various `*-requirements.txt` files in this repository.

## updating the distribution image

To update this image there's a couple of things that need to happen.

The files named `*-requirements.txt` in this repository are used by the
`rtoolkits-jupyterlab.def` file to install packages from different conda
channels. E.g. [conda-conda-forge-requirements.txt] is used like this: `conda
install --file conda-conda-forge-requirements.txt -c conda-forge` during the
build.

## updating a local image

default local image location: `/srv/singularity-images/`

default directory for this repository:
`/srv/singularity-images/src/singularity-jupyter`

To add packages to your jupyterlab instance after it is deployed you can edit
the `*-requirements.txt` files and then run the `build` script to regenerate
your singularity image.

## how it works

The singularity image is here:

```bash
/srv/singularity-images/rtoolkits-jupyterlab-new.sif
```

and that there are a couple bash shell scripts and a jupyter config file template  
that are also in that directory in addition to the `/etc/skel/` directory and by
virtue of that in the users home directory.

```bash
     jupyter_notebook_config.py
     findport-jupyterlab
     run-jupyterlab
```

Singularity overlays the host's unix filesystem except (by default) for the user homedir  
and /tmp, and we assume we are running from the user's homedir.

run-jupyterlab tells singularity to exec `findport-jupyterlab` .

`findport-jupyterlab` locates an unused port so tere will not be port collision for web access, 
tells the user how to connect via ssh port forwarding to the singularity container, generates a  
token to be used to authenticate to jupyterlab and launches jupyterlab within the container.
